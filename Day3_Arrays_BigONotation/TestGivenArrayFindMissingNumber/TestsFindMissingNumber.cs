using Microsoft.VisualStudio.TestPlatform.TestHost;

namespace TestGivenArrayFindMissingNumber
{
    [TestClass]
    public class TestsFindMissingNumber
    {
        [TestMethod]
        public void TestMethod1()
        {
            int[] arr = { 4, 2, 5, 7, 3 };
            int size = arr.Length;
            int number = Program.ReturnMissingNumber(arr, size);
            Assert.AreEqual(6, number);
        }
         [TestMethod]
        public void TestMethod3()
        {
            int[] arr = { 3, 1, 4 };
            int size = arr.Length;
            int number = Program.ReturnMissingNumber(arr, size);
            Assert.AreEqual(2, number); 
        }
         [TestMethod]
        public void TestMethod2()
        {
            int[] arr = { 103, 101 };
            int size = arr.Length;
            int number = Program.ReturnMissingNumber(arr, size);
            Assert.AreEqual(102, number);
        }
    }
}