namespace TestDataStructuresDay5
{
    [TestClass]
    public class UniqueNumbersTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            List<int> given = new() { 5, 2, 5, 4, 3 };
            List<int> actual = UniqueElementsClass.UniqueElements(given);
            List<int> result = new() {2,3,4,5};
            CollectionAssert.AreEqual(actual, result);
        }
        [TestMethod]
        public void TestMethod2()
        {
            List<int> given = new() { 1,1,1,1 };
            List<int> actual = UniqueElementsClass.UniqueElements(given);
            List<int> result = new() { 1 };
            CollectionAssert.AreEqual(actual, result);
        }
         [TestMethod]
        public void TestMethod3()
        {
            List<int> given = new() {};
            List<int> actual = UniqueElementsClass.UniqueElements(given);
            List<int> result = new() {};
            CollectionAssert.AreEqual(actual, result);
        }

    }

    [TestClass]
    public class RemoveMidElement
    {
        [TestMethod]
        public void TestMethod1()
        {
            LinkedList<int> given = new();
            given.AddLast(5);  
            given.AddLast(2);  
            given.AddLast(5);  
            given.AddLast(4);  
            given.AddLast(3);  

            EraseMiddleLL.removeMidElementOfLinkedList(given);
            LinkedList<int> result = new();
            result.AddLast(5);
            result.AddLast(2);
            result.AddLast(4);
            result.AddLast(3);
                
            CollectionAssert.AreEqual(given, result);
        }
        [TestMethod]
        public void TestMethod2()
        {
            LinkedList<int> given = new();
            given.AddLast(1);
            given.AddLast(1);
            given.AddLast(1);
            given.AddLast(1);
            given.AddLast(1);
            EraseMiddleLL.removeMidElementOfLinkedList(given);
            LinkedList<int> result = new();

            result.AddLast(1);
            result.AddLast(1);
            result.AddLast(1);
            result.AddLast(1);
            CollectionAssert.AreEqual(given, result);
        }
         [TestMethod]
        public void TestMethod3()
        {
            LinkedList<int> given = new() {};
            EraseMiddleLL.removeMidElementOfLinkedList(given);
            LinkedList<int> result = new() {};
            CollectionAssert.AreEqual(given, result);
        }

        [TestMethod]

        public void TestMethod4()
        {
            LinkedList<int> given = new() ;
            given.AddLast(1);
            given.AddLast(2);
            given.AddLast(3);
            given.AddLast(4);
            EraseMiddleLL.removeMidElementOfLinkedList(given);
            LinkedList<int> result = new();
            result.AddLast(1);
            result.AddLast(3);
            result.AddLast(4);
            CollectionAssert.AreEqual(given, result);
        }

    }
}