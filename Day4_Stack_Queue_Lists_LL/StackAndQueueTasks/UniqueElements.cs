﻿
public class UniqueElementsClass
{
    public static List<int> UniqueElements(List<int> givenList)
    {
        List<int> result = new List<int>();
        if (givenList.Count == 0)
        {
            return result;
        }
        givenList.Sort();
        result.Add(givenList.First());

        foreach (int element in givenList)
        {
            if (result.Last() != element)
            {
                result.Add(element);
            }
        }
        return result;
    }

    //static void Main()
    //{
        //List<int> l = new() { 5, 2, 5, 4, 3 };
        //List<int> result = UniqueElements(l);
        //Console.WriteLine("Result: [{0}]", string.Join(", ", result));
    //}
}